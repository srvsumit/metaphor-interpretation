import os
import os.path
import pickle
from os import path

class DataBaseManager:
    categories = None
    subjects = None

    pickeler = None
    cat_file = None
    sub_file = None

    def __init__(self):
        self.categories = {}
        self.subjects = {}
        self.cat_file = './data/categories.pkl'
        self.sub_file = './data/subject.pkl'

        self.load_records()

    def load_records(self):
        if path.exists(self.cat_file):
            with open(self.cat_file, 'rb') as f:
                self.categories = pickle.load(f)
        
        if path.exists(self.sub_file):
            with open(self.sub_file, 'rb') as f:
                self.subjects = pickle.load(f)

    def update_categories(self, categories):
        self.categories = categories

        with open(self.cat_file, 'wb') as f:
            pickle.dump(categories, f, pickle.HIGHEST_PROTOCOL)

    def update_subjects(self, subjects):
        self.subjects = subjects

        with open(self.sub_file, 'wb') as f:
            pickle.dump(subjects, f, pickle.HIGHEST_PROTOCOL)

    def get_categories(self):
        return self.categories

    def get_subjects(self):
        return self.subjects