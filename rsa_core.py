import torch
torch.set_default_dtype(torch.float64)  # double precision for numerical stability

import collections
import argparse
import matplotlib.pyplot as plt

import pyro
import pyro.distributions as dist
import pyro.poutine as poutine

from pyro.infer import Importance
from pyro.infer.abstract_infer import *

from search_inference import factor, Search, memoize, HashingMarginal, BestFirstSearch

from language import Language
import numpy as np

class RSA:
    
    categories = None
    category_probs = None
    
    utterances = None
    utterance_probs = None
    
    goals = None
    goal_probs = None
    
    featureSet = None
    featureSet_probs = None
    
    inference_num_sample = None
        
    lang = None
    
    def __init__(self, inference_num_sample=100):
        self.lang = Language()
        self.featureSet = {}
        self.featureSet_probs = {}
        print("Number of samples set to: " + str(inference_num_sample))
        
    def reset(self):        
        self.categories = []
        self.category_probs = []
        self.utterances = []
        self.utterance_probs = []
        self.goals = []
        self.goal_probs = []
        self.featureSet = {}
        self.featureSet_probs = {}
        
    def Marginal(fn):
#         return memoize(lambda *args: HashingMarginal(Importance(fn, num_samples=1000).run(*args)))
        return memoize(lambda *args: HashingMarginal(Search(fn).run(*args)))

    def get_categories(self):
        return (self.categories, self.category_probs)
    
    def set_categories(self, categories, category_probs):
        self.categories = categories
        self.category_probs = category_probs
    
    def get_utterances(self):
        return (self.utterances, self.utterance_probs)
    
    def set_utterances(self, utterances, utterance_probs):
        self.utterances = utterances
        self.utterance_probs = utterance_probs
     
    def get_goals(self):
        return (self.goals, self.goal_probs)
    
    def set_goals(self, goals, goal_probs):
        self.goals = goals
        self.goal_probs = goal_probs
    
    def set_featureSet(self, categories, featureSet, featureSet_probs):
        for idx, category in enumerate(categories):
            self.featureSet[category] = featureSet[idx]
            self.featureSet_probs[category] = featureSet_probs[idx]
        
    # It is extremely unlikely that John is actually a whale.
    def categoriesPrior(self):
        return self.categories[pyro.sample("category", dist.Categorical(probs=torch.tensor(self.category_probs)))]

    # The utterances are equally costly.
    def utterancePrior(self):
        return self.utterances[pyro.sample("utterance", dist.Categorical(probs=self.utterance_probs))]
    
    # Prior probability of speaker's goal is set to uniform but can
    # change with context/QUD.
    def goalPrior(self):
        return  self.goals[pyro.sample("goal", dist.Categorical(probs=torch.tensor(self.goal_probs)))]

    # information about feature priors (probabilistic world knowledge)
    # obtained by an experimental study (see paper)
    def featureSetPrior(self, utterance):
        smp = pyro.sample("feature", dist.Categorical(probs=torch.tensor(self.featureSet_probs[utterance])))
        return self.featureSet[utterance][smp]

    # Speaker optimality: alpha factor
    def getSpeakerOptimality(self):
        return 3.

    # Check if interpreted category is identical to utterance
    def literalInterpretation(self, utterance, category):
        return utterance == category

    @Marginal
    def literal_listener(self, utterance, goal):
        category = self.categoriesPrior()
        feature = self.featureSetPrior(category)
        factor("literal_meaning", 0. if self.literalInterpretation(utterance, category) else -999999.)        
        return (goal, feature)

    @Marginal
    def speaker(self, feature, goal):
        alpha = self.getSpeakerOptimality()
        utterance = self.utterancePrior()
        literal_marginal = self.literal_listener(utterance, goal)
        factor("listener", self.getSpeakerOptimality() * literal_marginal.log_prob((goal, feature)))
        return utterance

    """
    Pragmatic listener would receive an utterance with its POS. It evaluates the utterance for any prior information available and if there is any violation of properties of the Subject. It shall proceed to doing a Theory of Mind based evaluation only if there is any such violation. If the sentence is new, it would first set up the Subject if not already established from the prior sentences. If subject is already there, it just loads its category and properties.
    """
    @Marginal
    def pragmatic_listener(self, utterance):
        #TODO: Probably move this out so as to save on calculations by enabling memoize in Marginal back. High priority.
        category = self.categoriesPrior()
        feature = self.featureSetPrior(category)
        goal = self.goalPrior()
        speaker_marginal = self.speaker(feature, goal)
        factor("speaker", speaker_marginal.log_prob(utterance))
        return (category, feature)
