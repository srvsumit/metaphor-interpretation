import sys
sys.path.append('/mnt/Data/Installations/pip')

import nltk

from nltk.corpus import treebank
from nltk.corpus import wordnet as wn
from nltk.corpus import brown

import requests

class Language:
    concept_net_base_url = None
    person_synset = None
    
    def __init__(self):
        self.concept_net_base_url = 'http://api.conceptnet.io/query?'
        self.person_synset = wn.synset('person.n.01')
    
    def get_pos(self, sentence):
        tokens = nltk.word_tokenize(sentence)
        tagged_tokens = nltk.pos_tag(tokens)
        entities = nltk.chunk.ne_chunk(tagged_tokens)
        
        return tagged_tokens
    
    def get_base_form(self, word, pos):
        if 'VB' in pos:
            base_form = wn.morphy(word, wn.VERB)
        elif pos == 'NNS' or pos == 'NNPS':
            base_form = wn.morphy(word, wn.NOUN)
            
        if not base_form:
            return word    
        else:
            return base_form    
        
    def is_a_valid_synset(self, word):
        wordsyns = wn.synsets(word, pos=wn.NOUN)
        
        if wordsyns == None or not wordsyns:
            return False
        
        return True
    
    def is_a_person_hypernym(self, word):
        wordsyns = wn.synsets(word, pos=wn.NOUN)
        
        #Returns true if no synset is found for the given word assuming a proper noun not in dictionary is supposedly a human name.
        if wordsyns == None or not wordsyns:
            return True
        
        for syn in wordsyns:
            for hpn in syn.lowest_common_hypernyms(self.person_synset):
                for lemma in hpn.lemmas():
                    if lemma.name() == 'person':
                        return True
        return False

    def get_concept_net_pos(self, wordnet_pos):
        cn_pos = {
            "NN": "n",
            "NNP": "n",
            "VB": "v",
            "JJ": "a",
            "RB": "v"
        }
        
        return cn_pos.get(wordnet_pos, "Unsupported Wordnet POS")
    
    def get_properties_from_cn(self, word, pos):
        req_uri = self.concept_net_base_url  + "start=/c/en/"+ word.replace(' ', '_') + '&rel=/r/HasProperty&limit=5'
        response = requests.get(req_uri).json()
        
        properties = []
        for edge in response['edges']:
            prop = edge['end']
            properties.append((prop['label'], edge['weight']))
            
        return properties
    
    def get_capabilities_from_cn(self, word, pos):
        req_uri = self.concept_net_base_url + "start=/c/en/" + word.replace(' ', '_') + '&rel=/r/CapableOf&limit=5'
        response = requests.get(req_uri).json()
        
        capabilities = []
        for edge in response['edges']:
            capability = edge['end']
            capabilities.append((capability['label'], edge['weight']))
            
        return capabilities
    
    def get_locations_from_cn(self, word, pos):
        req_uri = self.concept_net_base_url + "start=/c/en/" + word.replace(' ', '_') + '&rel=/r/AtLocation&limit=5'
        response = requests.get(req_uri).json()
        
        locations = []
        for edge in response['edges']:
            location = edge['end']
            locations.append((location['label'], edge['weight']))
            
        return locations
    
    def get_word_forms_from_cn(self, word, pos, forms_explored):
        if word in forms_explored:
            return [] 
        forms_explored.append(word)
        
        req_uri = self.concept_net_base_url + "end=/c/en/" + word.replace(' ', '_') + '&rel=/r/FormOf&limit=5'
        response = requests.get(req_uri).json()
        for edge in response['edges']:
            form = edge['start']
            sense_label = form.get('sense_label')
            label = form['label']
            if sense_label is not None and sense_label != pos:
                continue
            self.get_word_forms_from_cn(label, pos, forms_explored)            
            
        return forms_explored
    
    def get_word_types_from_cn(self, word, types_explored, depth=0):
        if word in types_explored:
            return []
        types_explored.append(word)
        
        if depth == 2:
            return []
        
        depth_new = depth+1
        
        req_uri = self.concept_net_base_url + "start=/c/en/" + word.replace(' ', '_') + '&rel=/r/IsA&limit=3'
        response = requests.get(req_uri).json()
        for edge in response['edges']:
            form = edge['end']
            label = form['label']
            self.get_word_types_from_cn(label, types_explored, depth_new)            
            
        return types_explored
    

        