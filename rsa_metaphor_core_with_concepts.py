import torch
torch.set_default_dtype(torch.float64)  # double precision for numerical stability

import collections
import argparse
import matplotlib.pyplot as plt

import pyro
import pyro.distributions as dist
import pyro.poutine as poutine

from pyro.infer import Importance
from pyro.infer.abstract_infer import *

from search_inference import factor, Search, memoize, HashingMarginal, BestFirstSearch

from language import Language
import numpy as np

class RSA:
    
    categories = None
    category_probs = None
    
    utterances = None
    utterance_probs = None
    
    goals = None
    goal_probs = None
    
    featureSet = None
    featureSet_probs = None
    
    inference_num_sample = None
    
    subject = None
    subject_pos = None
    verb = None
    has_subject = False
    new_sentence = False
    obj = None
    has_object = False
    
    #Move DB to separate file. Very low priority.
    db_categories = None
    db_subjects = None
    
    lang = None
    
    def __init__(self, inference_num_sample=100):
        self.lang = Language()
        self.db_categories = {}
        self.db_subjects = {}
        self.featureSet = {}
        self.featureSet_probs = {}
        print("Number of samples set to: " + str(inference_num_sample))
        
    def reset(self):
#         self.lang = Language()
#         self.db_categories = {}
        self.db_subjects = {}
        
        self.subject = None
        self.subject_pos = None
        self.verb = None
        self.has_subject = False
        self.new_sentence = False
        self.obj = None
        self.has_object = False
        
    def Marginal(fn):
#         return memoize(lambda *args: HashingMarginal(Importance(fn, num_samples=1000).run(*args)))
        return lambda *args: HashingMarginal(Search(fn).run(*args))

    def get_db_categories(self):
        return self.db_categories
    
    def get_db_subjects(self):
        return self.db_subjects
    
    def get_categories(self):
        return (self.categories, self.category_probs)
    
    def set_categories(self, categories, category_probs):
        self.categories = categories
        self.category_probs = category_probs
    
    def get_subject(self):
        return self.subject
    
    def get_object(self):
        return self.obj
    
    def get_utterances(self):
        return (self.utterances, self.utterance_probs)
    
    def set_utterances(self, utterances, utterance_probs):
        self.utterances = utterances
        self.utterance_probs = utterance_probs
     
    def get_goals(self):
        return (self.goals, self.goal_probs)
    
    def set_goals(self, goals, goal_probs):
        self.goals = goals
        self.goal_probs = goal_probs
    
    def set_featureSet(self, utterance, featureSet, featureSet_probs):
        self.featureSet[utterance] = featureSet
        self.featureSet_probs[utterance] = featureSet_probs
        
    # It is extremely unlikely that John is actually a whale.
    def categoriesPrior(self):
        return self.categories[pyro.sample("category", dist.Categorical(probs=torch.tensor(self.category_probs)))]

    # The utterances are equally costly.
    def utterancePrior(self):
        return self.utterances[pyro.sample("utterance", dist.Categorical(probs=torch.tensor(self.utterance_probs)))]
    
    # Prior probability of speaker's goal is set to uniform but can
    # change with context/QUD.
    def goalPrior(self):
        return  self.goals[pyro.sample("goal", dist.Categorical(probs=torch.tensor(self.goal_probs)))]

    # information about feature priors (probabilistic world knowledge)
    # obtained by an experimental study (see paper)
    def featureSetPrior(self, utterance):
        smp = pyro.sample("feature", dist.Categorical(probs=torch.tensor(self.featureSet_probs[utterance])))
        return self.featureSet[utterance][smp]

    # Speaker optimality: alpha factor
    def getSpeakerOptimality(self):
        return 3.

    # Check if interpreted category is identical to utterance
    def literalInterpretation(self, utterance, category):
        return utterance == category

    @Marginal
    def literal_listener(self, utterance, goal):
        category = self.categoriesPrior()
        feature = self.featureSetPrior(category)
        factor("literal_meaning", 0. if self.literalInterpretation(utterance, category) else -999999.)        
        return (goal, feature)

    @Marginal
    def speaker(self, feature, goal):
        alpha = self.getSpeakerOptimality()
        utterance = self.utterancePrior()
        literal_marginal = self.literal_listener(utterance, goal)
        factor("listener", self.getSpeakerOptimality() * literal_marginal.log_prob((goal, feature)))
        return utterance

    """
    Pragmatic listener would receive an utterance with its POS. It evaluates the utterance for any prior information available and if there is any violation of properties of the Subject. It shall proceed to doing a Theory of Mind based evaluation only if there is any such violation. If the sentence is new, it would first set up the Subject if not already established from the prior sentences. If subject is already there, it just loads its category and properties.
    """
    @Marginal
    def pragmatic_listener(self, utterance):
        #TODO: Probably move this out so as to save on calculations by enabling memoize in Marginal back. High priority.
        category = self.categoriesPrior()
        feature = self.featureSetPrior(category)
        goal = self.goalPrior()
        speaker_marginal = self.speaker(feature, goal)
        factor("speaker", speaker_marginal.log_prob(utterance))
        return (category, feature)

    #Returns whether Pragmatic check required or not.
    def process_utterance_for_information(self, utterance, pos, tag):
#         print("has sub: " + str(self.has_subject))
#         print("sub: " + str(self.subject))
        utterance = utterance.lower()
        category = utterance
        
        if pos == 'NNS' or pos == 'NNPS':
            utterance = self.lang.get_base_form(utterance, pos)
            category = utterance
            pos = pos[:-1]
            
        if 'VB' in pos and tag == 'AUX':
            self.verb = self.lang.get_base_form(utterance, pos)
            return False
        
        if 'DT' in pos:
            return False
        
        if 'JJ' in pos:
            self.process_adjective_for_subject(utterance, pos)
            return False
            
        if utterance == '.' or pos == '.':
            self.new_sentence = True
            self.has_subject = False
            self.verb = None
            return False
            
        if pos == 'NNP' and not self.has_subject:
            self.new_sentence = False
            self.subject = utterance
            self.has_subject = True
            self.subject_pos = pos
        
        if 'PRP' in pos and self.subject:
            self.new_sentence = False
            self.has_subject = True
            return False
        
#         if 'PRP' in last_pos and 'NN' in pos and not self.subject:
#             self.new_sentence = False
#             self.has_subject = True
#             self.subject = utterance
        
        if pos == 'NN' and not self.has_subject:
            self.has_subject = True
            self.subject = utterance
            self.subject_pos = pos
            
        #TODO: Add case if no synset is found. High priority since testing on wider dataset relies on it.
        if pos == 'NNP' and self.lang.is_a_person_hypernym(utterance):
            category = "human"
            
        if pos == 'NN' and self.verb is not None and self.has_subject :
#             print("object processing: " + utterance)
            self.has_object = True
            self.obj = utterance
            return self.process_object_for_subject(utterance, pos)

        self.db_records_for_category(category, pos)
        self.db_records_for_subject(utterance, category)
        
        return False
    
    """
    Fetch subject's attributes and weights for their specific properties.
    """
    def process_adjective_for_subject(self, utterance, pos):
        if (not self.db_subjects):
            raise Exception("DB is empty of None.")
            
        property_of_subject = utterance
        if(self.verb == 'be'):
#             print("subject: " + str(self.subject))
            #TODO: The check for category match can possibly be removed. Check later.
            subject_records = self.db_subjects.get(self.subject)
            
            if (not subject_records):
                raise Exception('No record for subject in DB')
                
            subject_properties = subject_records.get('properties')
            if subject_properties is None:
                subject_properties = {'attributes':[], 'weights': []}
                self.db_subjects[self.subject]['properties'] = subject_properties
                
            attributes = subject_properties.get('attributes')
            attributes_weights = subject_properties.get('weights')

            if utterance not in attributes:
                attributes.append(utterance)
                attributes_weights.append(1.0)
            
            self.db_subjects[self.subject]['properties']['attributes'] = attributes
            self.db_subjects[self.subject]['properties']['weights'] = attributes_weights

        return True
    
    # Directly attributing utterance as the category. Considering every common noun is a category in itself.
    # TODO: Shall be modified to identify and add other relevant categories of the utterance (object) using Wordnet/ConceptNet.
    # TODO: Add the Object's category to the Subject's categories set temporarily for the pragmatic listener inference.
    # Possibly add the Object category to Subject category with a very small probability but add the goals with much higher probability to that of the subject.
    def process_object_for_subject(self, utterance, pos):
        category = utterance
        category_records = self.db_records_for_category(category, pos)
        
        goals = []
        
        if(self.verb == 'be'):
            #TODO: The check for category match can possibly be removed. Check later.
            subject_records = self.db_subjects.get(self.subject)
            subject_categories = subject_records['categories']
            
            if(utterance not in subject_categories):
                goals = category_records['properties'].get('attributes')
                goal_probs = category_records['properties'].get('probs')

                self.categories = subject_categories + [utterance]
                subject_category_probs = [x/1.1 for x in subject_records['probs']]
                prob_sum = 0
                for prob in subject_category_probs:
                    prob_sum += prob 
                subject_category_probs.append(1.0 - prob_sum)
                self.category_probs = subject_category_probs
                self.goals = goals
                self.goal_probs = goal_probs
                
                #Set utterances same as categories with equal probabilities.
                self.utterances = self.categories
                tensor_length = len(self.utterances)
                self.utterance_probs = torch.ones(tensor_length)/tensor_length
                return True
            
            return False
            
        return True
    
    #Check if subject record already present. If not, set its categories and respective properties.
    #For now, let it set up the subject categories every time.
    def db_records_for_subject(self, utterance, category):
        subject_record = self.db_subjects.get(utterance)
        if subject_record is None:
            subject_record = {}
            self.db_subjects[utterance] = subject_record
#         else:
#             return subject_record
            
        subject_categories = subject_record.get('categories')
        if subject_categories is None:
            subject_record['categories'] = []
            subject_record['probs'] = []

        subject_categories = subject_record.get('categories')
        if(category in subject_categories):
            return
        subject_categories.append(category)
        
        subject_probs = subject_record.get('probs')
        subject_probs.append(1.0)
        
        return subject_record
    
    #Check if category record already present. If not, fetch from concept net.
    def db_records_for_category(self, category, pos):
        db_category = self.db_categories.get(category)
        if (db_category is None):
            self.db_categories[category] = {}
        else:
            return db_category

        cn_pos = self.lang.get_concept_net_pos(pos)
        forms = self.lang.get_word_forms_from_cn(category, cn_pos, [])

        pros = []
        caps = []
        locs = []

        for form in forms:
            pros += self.lang.get_properties_from_cn(form, cn_pos)        
            caps += self.lang.get_capabilities_from_cn(form, cn_pos)
            locs += self.lang.get_locations_from_cn(form, cn_pos)

        self.db_categories[category]['properties'] = self.normalize_weights(pros)
        self.db_categories[category]['capabilities'] = self.normalize_weights(caps)
        self.db_categories[category]['locations'] = self.normalize_weights(locs)
        
        return self.db_categories[category]
      
    def normalize_weights(self, attributes):
        total_weight = 0.

        normalized_ = {}

        attribute_names = []
        attribute_probabilities = []
        for attribute, weight in attributes:
            total_weight += weight
            if attribute not in attribute_names:
                attribute_names.append(attribute)
                attribute_probabilities.append(weight)
            else:
                idx = attribute_names.index(attribute)
                attribute_probabilities[idx] += weight

        attribute_probabilities = [weight/total_weight for weight in attribute_probabilities]

        normalized_ = {"attributes": attribute_names, "probs": attribute_probabilities}

        return normalized_
   
