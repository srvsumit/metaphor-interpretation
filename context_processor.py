import torch
from language import Language
from database import DataBaseManager

from spacy import displacy
from collections import Counter
import en_core_web_sm
from spacy.symbols import *

torch.set_default_dtype(torch.float64)  # double precision for numerical stability

class ContextProcessor:
    subject = None
    subject_pos = None
    
    verb = None
    verb_pos = None
    
    has_subject = False
    new_sentence = False
    
    obj = None
    obj_pos = None
    has_object = False
    
    #Move DB to separate file. Very low priority.
    db_categories = None
    db_subjects = None
    
    adjective = None
    adj_pos = None
    
    lang = None
    db = None
    
    nlp = None

    def __init__(self):
        self.lang = Language()
        self.db = DataBaseManager()
        self.db_categories = self.db.get_categories()
        self.db_subjects = self.db.get_subjects()
        self.nlp = en_core_web_sm.load()

    def get_db_categories(self):
        return self.db_categories

    def update_db(self):
        self.db.update_categories(self.db_categories)
        self.db.update_subjects(self.db_subjects)
    
    def get_db_subjects(self):
        return self.db_subjects
    
    def get_categories(self):
        return (self.categories, self.category_probs)
    
    def get_subject(self):
        return self.subject, self.subject_pos
    
    def get_object(self):
        return self.obj, self.obj_pos
    
    def get_verb(self):
        return self.verb, self.verb_pos
    
    def get_adjective(self):
        return self.adjective, self.adj_pos
    
    def reset(self):
        if self.db_subjects:
            subject_records = self.db_subjects.get(self.subject)
            if subject_records:
                subject_records["properties"] = {"attributes":[], "weights": []}

        self.subject = None
        self.subject_pos = None
        self.verb = None
        self.verb_pos = None
        self.has_subject = False
        self.new_sentence = False
        self.obj = None
        self.obj_pos = None
        self.has_object = False
        self.adjective = None
        self.adj_pos = None
    
    def process_subtree(self, token):
        final_token = ""
        for child in token:
            if ('DT' not in child.tag_ and 'PRP' not  in child.tag_ and 'CC' not in child.tag_ and 'JJ' not in child.tag_):
                final_token = final_token + " " + child.lemma_
        return final_token

    def get_subject_and_object_using_spacy(self, sentence):
        doc = self.nlp(sentence)

        phrases = []
        
        for chunk in doc.noun_chunks:
            ph = self.process_subtree(chunk.subtree)
            add_ph = True
            for phrs in phrases:
                if ph in phrs:
                    add_ph = False
            if add_ph:
                phrases.append(ph)
        if not phrases:
            return "", [""]
        return phrases[0], phrases[1:]


    def process_utterance_for_information(self, utterance, pos, tag):
        utterance = utterance.lower()
        category = utterance
        
        if pos == 'NNS' or pos == 'NNPS':
            utterance = self.lang.get_base_form(utterance, pos)
            category = utterance
            pos = pos[:-1]
            
        if 'VB' in pos and tag == 'AUX':
            self.verb = self.lang.get_base_form(utterance, pos)
            self.verb_pos = pos
            return 
        
        if 'DT' in pos:
            return
        
        if 'JJ' in pos:
            self.adjective = utterance
            self.adj_pos = pos
            return 
                
        if pos == 'NNP' and not self.has_subject:
            self.new_sentence = False
            self.subject = utterance
            self.has_subject = True
            self.subject_pos = pos
        
        if 'PRP' in pos and self.subject:
            self.new_sentence = False
            self.has_subject = True
            return
        
#         if 'PRP' in last_pos and 'NN' in pos and not self.subject:
#             self.new_sentence = False
#             self.has_subject = True
#             self.subject = utterance
        
        if pos == 'NN' and not self.has_subject:
            self.has_subject = True
            self.subject = utterance
            self.subject_pos = pos
            
        if pos == 'NN' and self.verb is not None and self.has_subject :
#             print("object processing: " + utterance)
            self.has_object = True
            self.obj = utterance
            self.obj_pos = pos
            return 
        
        return
    
    def get_category_for_utterance(self, utterance, pos):
        #TODO: Add case for "no synset found". High priority since testing on wider dataset relies on it.
        if pos == 'NNP' and self.lang.is_a_person_hypernym(utterance):
            category = "human"
        else:
            category = utterance
        
        return category
    
    def get_categories_for_utterance(self, utterance, pos):
        #TODO: Add case for "no synset found". High priority since testing on wider dataset relies on it.
        if pos == 'NNP' and self.lang.is_a_person_hypernym(utterance):
            category = "human"
        else:
            category = utterance
        
        categories = self.lang.get_word_types_from_cn(category, [], 0)
        
        return categories
            

    """
    Fetch subject's attributes and weights for their specific properties.
    """
    def process_adjective_for_subject(self):
        if (not self.db_subjects):
            raise Exception("DB is empty of None.")
        
        if not self.adjective:
            return
    
        property_of_subject = self.adjective
        
        if(self.verb == 'be'):
#             print("subject: " + str(self.subject))
            #TODO: The check for category match can possibly be removed. Check later.
            subject_records = self.db_subjects.get(self.subject)
            
            if (not subject_records):
                raise Exception('No record for subject in DB')
                
            subject_properties = subject_records.get("properties")
            if subject_properties is None:
                subject_properties = {"attributes":[], "weights": []}
                self.db_subjects[self.subject]["properties"] = subject_properties
                
            attributes = subject_properties.get("attributes")
            attributes_weights = subject_properties.get("weights")

            if property_of_subject not in attributes:
                attributes.append(property_of_subject)
                attributes_weights.append(1.0)
            
            self.db_subjects[self.subject]["properties"]["attributes"] = attributes
            self.db_subjects[self.subject]["properties"]["weights"] = attributes_weights

        return True
    
    def is_sub_obj_category_violated(self):
        if(self.verb == 'be'):
            category = self.obj
            pos = self.obj_pos

            subject_records = self.db_subjects.get(self.subject)
            subject_categories = subject_records["categories"]

            if(category not in subject_categories):
                return True
        
        return False
    
    """ 
    Retrieves properties and capabilities of the utterance to be used as the featureset.
    Additionally, if the subject has been attributed some specific properties, they are added(if not already present) and probabilities of these specific properties are increased in the featureset to give them higher weight.
    Subject properties are added for every incoming utterance as subject properties are added locally to the subject instance and is independent of the utterance" category.
    """
    def get_feature_set_for_category(self, category):
        records = self.db_categories.get(category)
        
        feature_probs = []
        
        category_properties = records["properties"].get("attributes")
        property_feature_probs = records["properties"].get("probs")
  
        category_capabilities = records["capabilities"].get("attributes")
        capability_feature_probs = records["capabilities"].get("probs")
        
        subject_records = self.db_subjects.get(self.subject)
        subject_categories = subject_records.get("categories")
        subject_properties = subject_records.get("properties")
        
        featureSet = [] + category_properties + category_capabilities
        feature_probs = [] + property_feature_probs + capability_feature_probs
        
        (featureSet, feature_probs) = self.normalize_probs(featureSet, feature_probs)
        
        if subject_properties is not None:
            subject_attributes = subject_properties.get("attributes")
            subject_attributes_weights = subject_properties.get("weights")

            feature_probs = [x/1.1 for x in feature_probs]

            #If subject attribute already present in feature set, do not add again.
            for i, attribute in enumerate(subject_attributes):
                if attribute in featureSet:
                    idx = featureSet.index(attribute)
                    feature_probs[idx] = subject_attributes_weights[i]
                    continue
                featureSet.append(attribute)
                feature_probs.append(subject_attributes_weights[i]) 
            
            feature_probs_sum = 0
            for prob in feature_probs:
                feature_probs_sum += prob 

            feature_probs = [x/feature_probs_sum for x in feature_probs]

        return featureSet, feature_probs
    
    def get_feature_set_for_categories(self, categories):
        
        featureSet_full = []
        feature_probs_full = []
        
        for category in categories:
            featureSet, feature_probs = self.get_feature_set_for_category(category)
            
            featureSet_full.append(featureSet)
            feature_probs_full.append(feature_probs)

        return featureSet_full, feature_probs_full
        
        
    # Directly attributing utterance as the category. Considering every common noun is a category in itself.
    # TODO: Shall be modified to identify and add other relevant categories of the utterance (object) using Wordnet/ConceptNet.
    # TODO: Add the Object's category to the Subject's categories set temporarily for the pragmatic listener inference.
    # Possibly add the Object category to Subject category with a very small probability but add the goals with much higher probability to that of the subject.
    def process_object_for_subject(self):
        category = self.obj
        pos = self.obj_pos
        
        category_records = self.db_records_for_category(category, pos)
        
        goals = []
        
        if(self.verb == 'be'):
            #TODO: The check for category match can possibly be removed. Check later.
            subject_records = self.db_subjects.get(self.subject)
            subject_categories = subject_records["categories"]
            
            property_goals = category_records["properties"].get("attributes")
            property_goal_probs = category_records["properties"].get("probs")
            
            capability_goals = category_records["capabilities"].get("attributes")
            capability_goal_probs = category_records["capabilities"].get("probs")

            goals = property_goals + capability_goals
            goal_probs = property_goal_probs + capability_goal_probs
            
            goals, goal_probs = self.normalize_probs(goals, goal_probs)
            categories = subject_categories + [category]
            
            subject_category_probs = [x/1.1 for x in subject_records["probs"]]
            
            prob_sum = 0
            for prob in subject_category_probs:
                prob_sum += prob 
            subject_category_probs.append(1.0 - prob_sum)
            category_probs = subject_category_probs
        
            #Set utterances same as categories with equal probabilities.
            utterances = categories
            tensor_length = len(utterances)
            utterance_probs = torch.ones(tensor_length)/tensor_length
            
        return categories, subject_category_probs, utterances, utterance_probs, goals, goal_probs
    
    #Check if subject record already present. If not, set its categories and respective properties.
    #For now, let it set up the subject categories every time.
    def db_records_for_subject(self, utterance, category):
        subject_record = self.db_subjects.get(utterance)
        if subject_record is None:
            subject_record = {}
            self.db_subjects[utterance] = subject_record
        else:
            return subject_record
            
        subject_categories = subject_record.get("categories")
        if subject_categories is None:
            subject_record["categories"] = []
            subject_record["probs"] = []

        subject_categories = subject_record.get("categories")
        if(category in subject_categories):
            return
        subject_categories.append(category)
        
        subject_probs = subject_record.get("probs")
        subject_probs.append(1.0)
        
        self.update_db()

        return subject_record
    
    def db_records_for_subject_multiple_categories(self, utterance, categories):
        subject_record = self.db_subjects.get(utterance)
        if subject_record is None:
            subject_record = {}
            self.db_subjects[utterance] = subject_record
        else:
            return subject_record
            
        subject_categories = subject_record.get("categories")
        if subject_categories is None:
            subject_record["categories"] = []
            subject_record["probs"] = []

        subject_categories = subject_record.get("categories")
        
        subject_probs = subject_record.get("probs")
            
        for category in categories:
            if(category in subject_categories):
                continue
            subject_categories.append(category)
            subject_probs.append(1.0/len(categories))

        self.update_db()
        
        return subject_record
    
    #Check if category record already present. If not, fetch from concept net.
    def db_records_for_category(self, category, pos):
        db_category = self.db_categories.get(category)
        if not db_category:
            self.db_categories[category] = {}
        else:
            return db_category

        cn_pos = self.lang.get_concept_net_pos(pos)
        forms = self.lang.get_word_forms_from_cn(category, cn_pos, [])
        
        pros = []
        caps = []
        locs = []

        for form in forms:
            pros += self.lang.get_properties_from_cn(form, cn_pos)        
            caps += self.lang.get_capabilities_from_cn(form, cn_pos)
            locs += self.lang.get_locations_from_cn(form, cn_pos)

        self.db_categories[category]["properties"] = self.normalize_weights(pros)
        self.db_categories[category]["capabilities"] = self.normalize_weights(caps)
        self.db_categories[category]["locations"] = self.normalize_weights(locs)
        
        self.update_db()

        return self.db_categories[category]
      
    #Check if category record already present. If not, fetch from concept net.
    def db_records_for_categories(self, categories, pos):
        category_records = [] 
        for category in categories:
            record = self.db_records_for_category(category, pos)
            category_records += record
        
        return category_records
    
    def normalize_weights(self, attributes):
        total_weight = 0.

        normalized_ = {}

        attribute_names = []
        attribute_probabilities = []
        for attribute, weight in attributes:
            total_weight += weight
            if attribute not in attribute_names:
                attribute_names.append(attribute)
                attribute_probabilities.append(weight)
            else:
                idx = attribute_names.index(attribute)
                attribute_probabilities[idx] += weight

        attribute_probabilities = [weight/total_weight for weight in attribute_probabilities]

        normalized_ = {"attributes": attribute_names, "probs": attribute_probabilities}

        return normalized_
    
    def normalize_probs(self, attributes, attribute_probs):
        total_weight = 0.

        normalized_ = {}

        attribute_names = []
        attribute_probabilities = []
        for attribute, weight in zip(attributes, attribute_probs):
            total_weight += weight
            if attribute not in attribute_names:
                attribute_names.append(attribute)
                attribute_probabilities.append(weight)
            else:
                idx = attribute_names.index(attribute)
                attribute_probabilities[idx] += weight

        attribute_probabilities = [weight/total_weight for weight in attribute_probabilities]

        return attribute_names, attribute_probabilities

    def set_new_sentence(self):
        self.has_subject = False
        self.new_sentence = True
        self.verb = None
        self.verb_pos = None
        self.adjective = None
        self.adj_pos = None
        return
