#!/usr/bin/env python
# coding: utf-8

# In[1]:


#first some imports
import torch
torch.set_default_dtype(torch.float64)  # double precision for numerical stability

import collections
import argparse
import matplotlib.pyplot as plt

import pyro
import pyro.distributions as dist
import pyro.poutine as poutine

from pyro.infer import Importance
from pyro.infer.abstract_infer import *

from search_inference import factor, Search, memoize, HashingMarginal, BestFirstSearch


class RSA:
    
    categories = None
    utterances = None
    goals = None
    featureSets = None
    inference_num_sample = None
    
    def __init__(self, inference_num_sample=100):
        print("Number of samples set to: " + str(inference_num_sample))
        #This is the world prior
        # John could either be a whale or a person.
        self.categories = ["whale", "person"]

        # The speaker could either say "John is a whale" or "John is a person."
        self.utterances = ["whale", "person"]


        # Speaker's possible goals are to communicate feature 1, 2, or 3
        self.goals = ["large", "graceful", "majestic"]

        # Related features with the world:
        # The features of John being considered are "large", "graceful",
#         # "majestic." Featu00res are binary.
        # "majestic." Featu00res are binary.
        large = 1
        graceful= 1
        majestic = 1

        self.featureSets = [
          {"large" : 1, "graceful" : 1, "majestic" : 1},
          {"large" : 1, "graceful" : 1, "majestic" : 0},
          {"large" : 1, "graceful" : 0, "majestic" : 1},
          {"large" : 1, "graceful" : 0, "majestic" : 0},
          {"large" : 0, "graceful" : 1, "majestic" : 1},
          {"large" : 0, "graceful" : 1, "majestic" : 0},
          {"large" : 0, "graceful" : 0, "majestic" : 1},
          {"large" : 0, "graceful" : 0, "majestic" : 0}
        ]

    def Marginal(fn):
#         return memoize(lambda *args: HashingMarginal(Importance(fn, num_samples=1000).run(*args)))
        return memoize(lambda *args: HashingMarginal(Search(fn).run(*args)))

    
    # It is extremely unlikely that John is actually a whale.
    def categoriesPrior(self):
        return self.categories[pyro.sample("category", dist.Categorical(probs=torch.tensor([0.01, 0.99])))]

    # The utterances are equally costly.
    def utterancePrior(self):
        tensor_length = len(self.utterances)
        probs = torch.ones(tensor_length) /tensor_length
        return self.utterances[pyro.sample("utterance", dist.Categorical(probs=probs))]
    
    # Prior probability of speaker's goal is set to uniform but can
    # change with context/QUD.
    def goalPrior(self):
        tensor_length = len(self.goals)
        probs = torch.ones(tensor_length) / tensor_length
        return  self.goals[pyro.sample("goal", dist.Categorical(probs=probs))]


    # information about feature priors (probabilistic world knowledge)
    # obtained by an experimental study (see paper)
    def featureSetPrior(self, category):
        if category == "whale":
            smp = pyro.sample("feature", dist.Categorical(probs=torch.tensor([0.30592786494628, 0.138078454222818,
                                          0.179114768847673, 0.13098781834847,
                                          0.0947267162507846, 0.0531420411185539,
                                          0.0601520520596695, 0.0378702842057509])))
            return self.featureSets[smp]
        if category == "person":
            smp = pyro.sample("feature", dist.Categorical(probs=torch.tensor([0.11687632453038, 0.105787535267869,
                                           0.11568145784997, 0.130847056136141,
                                           0.15288225956497, 0.128098151176801,
                                           0.114694702836614, 0.135132512637255])))
            return self.featureSets[smp]
        return True

    # Speaker optimality: alpha factor
    def getSpeakerOptimality(self):
        return 3.

    # Check if interpreted category is identical to utterance
    def literalInterpretation(self, utterance, category):
        return utterance == category

    # Check if goal is satisfied
    def goalState(self, goal, featureSet):
        if goal == "large": 
            return featureSet["large"]
        if goal == "graceful": 
            return featureSet["graceful"]
        if goal == "majestic": 
            return featureSet["majestic"]
        return True

#     @Marginal
#     def project(self, dist, qud):
#         v = pyro.sample("proj", dist)
#         factor("listener", alpha * dist.log_prob(qud).exp().item())
#         return featureSets[qud]

    @Marginal
    def literal_listener(self, utterance, goal):
        category = self.categoriesPrior()
        featureSet = self.featureSetPrior(category)
        factor("literal_meaning", 0. if self.literalInterpretation(utterance, category) else -999999.)
        return self.goalState(goal, featureSet)

    @Marginal
    def speaker(self, large, graceful, majestic, goal):
        alpha = self.getSpeakerOptimality()
        utterance = self.utterancePrior()
        literal_marginal = self.literal_listener(utterance, goal)
        qud = self.goalState(goal, {"large" : large, "graceful" : graceful, "majestic" : majestic})
        factor("listener", self.getSpeakerOptimality() * literal_marginal.log_prob(qud))
        return utterance

    @Marginal
    def pragmatic_listener(self, utterance):
        category = self.categoriesPrior()
        featureSet = self.featureSetPrior(category)
        large = featureSet["large"]
        graceful = featureSet["graceful"]
        majestic = featureSet["majestic"]
        goal = self.goalPrior()
        speaker_marginal = self.speaker(large, graceful, majestic, goal)
        factor("speaker", speaker_marginal.log_prob(utterance))
        return (category, large, graceful, majestic)

    def plot_dist(self, d):
        support = d.enumerate_support()
        data = [d.log_prob(s).exp().item() for s in d.enumerate_support()]
        names = list(map(str, support))

        plt.figure
        ax = plt.subplot(111)
        width = 0.3
        bins = [x-width/2 for x in range(1, len(data) + 1)]
        ax.bar(bins,data,width=width)
        ax.set_xticks(list(range(1, len(data) + 1)))
        ax.set_xticklabels(names, rotation=45, rotation_mode="anchor", ha="right")
        plt.show()
